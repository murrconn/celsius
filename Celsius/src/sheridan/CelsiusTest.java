package sheridan;

import static org.junit.Assert.*;
import org.junit.Test;


public class CelsiusTest {
	int num = 85;
	@Test
	public void testfromFahrenheit()  {
		assertFalse("Must be in celsius", Celsius.fromFahrenheit(num) == 90);
	}
	@Test
	public void testfromFahrenheitException()  {
		assertFalse("must be less than 900", Celsius.fromFahrenheit(900) == 100);
	}
	@Test
	public void testfromFahrenheitBoundaryIn()  {
		assertFalse("Must be real", Celsius.fromFahrenheit(19919) == 90);
	}
	@Test
	public void testfromFahrenheiBoundaryOut()  {
		assertFalse("Must have a temperature", Celsius.fromFahrenheit(0000) == 90);
	}
}
